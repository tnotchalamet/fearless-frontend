
function createErrorMessage(message) {
  return `
  <div class="alert alert-danger" role="alert">
  ${message}
</div>`


}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const error = document.querySelector(".row")
            const message = "Unable to fetch"
            error.innerHTML = createErrorMessage(message)
        } else {
          const data = await response.json();

          const formTag = document.getElementById('create-location-form');
          formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            } else {
                const errorCatch = document.querySelector('.row')
                const apierror = "Error"
                const message = createErrorMessage(apierror)
                errorCatch.innerHTML = message
            }
          });


          const selectTag = document.getElementById('state');
          for (let state of data.states) {
            const option = document.createElement('option')
            option.value = state.abbreviation;
            option.innerHTML = state.name
            selectTag.appendChild(option)
          }


          ; }

      } catch (e) {
        const errorCatch = document.querySelector('.row')
        const message = createErrorMessage(e)
        errorCatch.innerHTML = message
      }
});
