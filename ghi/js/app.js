function createCard(name, description, pictureUrl, confStart, confEnd, location) {
    return `
      <div class="card shadow-lg mb-3 w-5">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-secondary">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer bg-light border-white">${confStart}-${confEnd}</div>
      </div>
    `;
  }
  function createErrorMessage(message) {
    return `
    <div class="alert alert-danger" role="alert">
    ${message}
  </div>`


  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const error = document.querySelector(".row")
            const message = "Unable to fetch"
            error.innerHTML = createErrorMessage(message)
        } else {
          const data = await response.json();

          const card = document.querySelectorAll('.col');
          let i = 0
          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const name = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const confStart = new Date(details.conference.starts).toLocaleDateString();
              const confEnd = new Date(details.conference.ends).toLocaleDateString();
              const location = details.conference.location.name;
              const html = createCard(name, description, pictureUrl, confStart, confEnd, location);
              card[i % 3].innerHTML += html;
              i++
            }
          }


        }

      } catch (e) {
        const errorCatch = document.querySelector('.row')
        const message = createErrorMessage(e)
        errorCatch.innerHTML = message
      }
});
