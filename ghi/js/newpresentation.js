function createErrorMessage(message) {
    return `
    <div class="alert alert-danger" role="alert">
    ${message}
  </div>`

  function getSelectedConferenceId() {
    const selectElement = document.getElementById('conferenceSelect');
    return selectElement.value; //
  }

  }

  window.addEventListener('DOMContentLoaded', async () => {
      const url = `http://localhost:8000/api/conferences/`;

      try {
          const response = await fetch(url);

          if (!response.ok) {
              const error = document.querySelector(".row")
              const message = "Unable to fetch"
              error.innerHTML = createErrorMessage(message)
          } else {
            const data = await response.json();
            console.log(data);


          }
        } catch (e) {
            const errorCatch = document.querySelector('.row')
            const message = createErrorMessage(e)
            errorCatch.innerHTML = message
          }});
