function createErrorMessage(message) {
    return `
    <div class="alert alert-danger" role="alert">
    ${message}
  </div>`


  }

  window.addEventListener('DOMContentLoaded', async () => {

      const url = 'http://localhost:8000/api/locations/';

      try {
          const response = await fetch(url);

          if (!response.ok) {
              const error = document.querySelector(".row")
              const message = "Unable to fetch"
              error.innerHTML = createErrorMessage(message)
          } else {
            const data = await response.json();
            console.log(data);

            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
              event.preventDefault();
              const formData = new FormData(formTag);
              const json = JSON.stringify(Object.fromEntries(formData));
              const conferencesUrl = 'http://localhost:8000/api/conferences/';
              const fetchConfig = {
              method: "post",
              body: json,
              headers: {
                  'Content-Type': 'application/json',
              },
              };
              const response = await fetch(conferencesUrl, fetchConfig);
              if (response.ok) {
              formTag.reset();
              const newConference = await response.json();
              } else {
                  const errorCatch = document.querySelector('.row')
                  const apierror = "Error"
                  const message = createErrorMessage(apierror)
                  errorCatch.innerHTML = message
              }
            });


            const selectTag = document.getElementById('location');
            for (let location of data.locations) {
              const option = document.createElement('option')
              option.value = location.id;
              option.innerHTML = location.id;
              selectTag.appendChild(option)
            }


            ; }

        } catch (e) {
          const errorCatch = document.querySelector('.row')
          const message = createErrorMessage(e)
          errorCatch.innerHTML = message
        }
  });
